package br.com.mobitbrasil.interview.service;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import org.apache.deltaspike.jpa.api.transaction.Transactional;

import br.com.mobitbrasil.interview.model.Contact;
import br.com.mobitbrasil.interview.repository.ContactRepository;
import io.vavr.control.Option;

@Transactional
public class ContactService {

	@Inject
	private ContactRepository repository;

	public Function<Integer, Contact> findById = id -> Option.of(repository.findBy(id)).getOrElseThrow(NoResultException::new);

	public Supplier<List<Contact>> findFirst10 = () -> repository.findFirst10OrderByNameAsc();

	public Function<Contact, Contact> save = contact -> repository.saveAndFlush(contact);

	public Consumer<Contact> delete = contact -> repository.remove(contact);

}

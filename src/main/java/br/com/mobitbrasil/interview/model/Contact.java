package br.com.mobitbrasil.interview.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Contact extends BaseEntity<Integer> {

	private static final long serialVersionUID = -1500922989150835052L;

	@Column(nullable = false)
	@Size(max = 100)
	private String name;

	@Column(nullable = false, unique = true)
	@Size(max = 100)
	@Email
	private String email;

	private String address;

	@Column(nullable = false)
	@NotEmpty
	private String phones;

	public Contact() {

	}

	public Contact(Integer id) {
		super.setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

}

package br.com.mobitbrasil.interview.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/check")
@Produces("application/json;charset=UTF-8")
@Consumes("application/json;charset=UTF-8")
public class CheckRestEndpoint {

	@GET
	public Response get() {
		return Response.ok("Success").build();
	}

}

package br.com.mobitbrasil.interview.rest;

import java.io.IOException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.mobitbrasil.interview.model.Contact;
import br.com.mobitbrasil.interview.service.ContactService;

@Stateless
@Path("/contact")
@Produces("application/json;charset=UTF-8")
@Consumes("application/json;charset=UTF-8")
public class ContactRestEndpoint {

	@Inject
	private ContactService contactService;

	@GET
	@Path("{id}")
	public Response findById(@PathParam("id") Integer id) throws IOException {
		return Response.ok(contactService.findById.apply(id)).build();
	}

	@GET
	public Response findTop10() {
		return Response.ok(contactService.findFirst10.get()).build();
	}

	@POST
	public Response save(Contact contact) throws IOException {
		return Response.ok(contactService.save.apply(contact)).build();
	}

	@PUT
	public Response update(Contact contact) throws IOException {
		return Response.ok(contactService.save.apply(contact)).build();
	}

	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") Integer id) throws IOException {
		Contact contact = contactService.findById.apply(id);

		contactService.delete.accept(contact);

		return Response.ok(contact).build();
	}

}

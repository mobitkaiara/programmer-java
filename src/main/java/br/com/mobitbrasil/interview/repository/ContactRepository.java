package br.com.mobitbrasil.interview.repository;

import java.util.List;

import org.apache.deltaspike.data.api.AbstractEntityRepository;
import org.apache.deltaspike.data.api.Repository;

import br.com.mobitbrasil.interview.model.Contact;

@Repository
public abstract class ContactRepository extends AbstractEntityRepository<Contact, Integer> {

	public abstract List<Contact> findFirst10OrderByNameAsc();

}

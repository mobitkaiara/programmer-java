## Mobit

O projeto foi construído utilizando Maven e é executado no servidor de aplicação Wildfly.

**Configuração do datasource**

    <datasource jndi-name="java:jboss/datasources/ProgrammerDS" pool-name="ProgrammerDS">
        <connection-url>jdbc:postgresql://localhost:5432/programmer</connection-url>
        <driver>postgres</driver>
        <security>
            <user-name>postgres</user-name>
            <password>postgres</password>
        </security>
        <timeout>
            <blocking-timeout-millis>60000</blocking-timeout-millis>
            <idle-timeout-minutes>20</idle-timeout-minutes>
        </timeout>
        <statement>
            <prepared-statement-cache-size>128</prepared-statement-cache-size>
            <share-prepared-statements>true</share-prepared-statements>
        </statement>
    </datasource>

Obs: É necessário ter o driver do PostgreSQL dentro do Wildfly.

**Banco de dados**

O banco de dados utlizado foi o **PostgreSQL**

É necessário criar um database de nome: **"programmer"** previamente.